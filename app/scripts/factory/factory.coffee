angular.module('topNewsApp').factory( "NewsList", ($http) ->
  getItems: ->
    url = "http://pipes.yahoo.com/pipes/pipe.run?_id=DqsF_ZG72xGLbes9l7okhQ&_render=json&_callback=JSON_CALLBACK"
    $http({method: 'JSONP', url: url})
)

angular.module('topNewsApp').filter "to_trusted", [ "$sce", ($sce) ->
  (text) ->
    $sce.trustAsHtml text
]
