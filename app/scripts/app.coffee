'use strict'

###*
 # @ngdoc overview
 # @name topNewsApp
 # @description
 # # topNewsApp
 #
 # Main module of the application.
###
angular
  .module('topNewsApp', [])
