'use strict'

###*
 # @ngdoc function
 # @name topNewsApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the topNewsApp to display top ten news from youtube
###
angular.module('topNewsApp')
  .controller 'MainCtrl', ($scope, NewsList) ->

    handleNews = (data, status) ->
      $scope.news = data.value.items
      $scope.news.error = false
      $scope.totalCount = data.count

    handleError = ->
      $scope.news =
        error : true

    $scope.limitNumber = 10
    $scope.seeMore = (limitNumber) ->
      if (limitNumber + 10) < $scope.totalCount
        $scope.limitNumber = limitNumber + 10
      else
        $scope.limitNumber = $scope.totalCount

    NewsList.getItems().success(handleNews).error(handleError)





